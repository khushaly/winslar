import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {LocationStrategy, CommonModule, HashLocationStrategy} from '@angular/common';
import { ChartsModule } from 'ng2-charts';
import { AppComponent } from './app.component';
// Routing Module
import { AppRoutingModule } from './app.routing';
import { FullLayoutComponent} from './layout/full-layout.component';
import {Ng2AutoBreadCrumb} from "ng2-auto-breadcrumb";
import { HomeComponent } from './home/home.component';
import { SectionComponent } from './section/section.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {Ng2SmartTableModule} from "ng2-smart-table";
import {NgxPaginationModule} from 'ngx-pagination';
import { OrderModule } from 'ngx-order-pipe';
import {ViewComponent} from 'app/components/ViewComponent/ViewComponent.component';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    Ng2AutoBreadCrumb,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    Ng2SmartTableModule,
    NgxPaginationModule,
    OrderModule
  ],
  declarations: [
    FullLayoutComponent,
    AppComponent,
    HomeComponent,
    SectionComponent,
    ViewComponent
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
