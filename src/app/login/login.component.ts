import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {ApiServiceProvider} from '../Service/api_service';
import {BaseURI} from '../Service/Constants';
declare var $:any;
@Component({
  templateUrl: './login.component.html',
  selector:'login',
  providers:[ApiServiceProvider]

})
export class LoginComponent {

  public form:FormGroup;
  public email:AbstractControl;
  public password:AbstractControl;
  public submitted:boolean = false;
  userName:any;
  setusername = localStorage.getItem('setname');
  setpass =  localStorage.getItem('password');
  constructor(public api:ApiServiceProvider,private fb:FormBuilder,private router:Router) {
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/)])]
    });
    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];


  }

  public onSubmit(values:any):void {
    
    let login_uri = BaseURI+'login';
    localStorage.setItem('User_Name', values.userName);
    localStorage.setItem('password', values.password);
    this.api.API_post(login_uri, values).subscribe(data=>{
      localStorage.setItem('User_Name', data.login);
      localStorage.setItem('User_Status', data.status);
      localStorage.setItem('User_Login_Seq', data.loginSeq);
      this.router.navigate(['/home']);
     // this.api.AlertMessage('Login Success','Loggedin Successfully','success');
    },err=>{
      this.api.AlertMessage('Error while login','err:'+err,'error');
    })


        if (values.remember) {

          
          localStorage.setItem('setname', values.userName);
          localStorage.setItem('setpass', values.password);
        } 


  }





  public ForgotPass(formvalues){
    
    let forgoturl = BaseURI+'forgotPassword';
    //this.api.API_FORM_POST(forgoturl,formvalues.value).subscribe(data=>{
      this.api.API_post(forgoturl, formvalues).subscribe(data=>{  
        $('#Model_forgot').modal('hide');

        this.api.AlertMessage('Request Sent Successfully  ','Forgot password request sent successfully, please check mail','success');

    },err=>{
      $('#Model_forgot').modal('hide');
      this.api.AlertMessage('Error occured','err:'+err,'error');
    })
  }

}
