import {Component} from '@angular/core';
import {ApiServiceProvider} from '../Service/api_service';
import {BaseURI} from '../Service/Constants';
import { Router } from '@angular/router';   
declare var $:any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
  styleUrls:['./full-layout.component.scss'],
  providers:[ApiServiceProvider]
})
export class FullLayoutComponent{

  public toggleBarIcon:boolean=true;
  public NavMenus = [];
  dateString:any;
  UserProfileData = [];
  constructor(public api:ApiServiceProvider , public router:Router){
    this.dateString =new Date().getTime();
    let navmenus_url = BaseURI+'userMenus';

    var status = localStorage.getItem('User_Status');

		if(status == "1"){
    this.api.API_get_auth(navmenus_url).subscribe(data=>{
      this.NavMenus = data;
    },err=>{

    })

    this.getuserinfo();
  }

  }



  toggle():void{
    let self=this;
    setTimeout(()=>{
      self.toggleBarIcon=!self.toggleBarIcon;

    },500)
  }

  public ResetPass(formvalues){
    
    var uniqueId = localStorage.getItem('uniqueId');
    let forgoturl = BaseURI+'doResetPassword';
    formvalues.value.uniqueId = uniqueId;
    this.api.API_post(forgoturl,formvalues.value).subscribe(data=>{
        $('#Model_reset').modal('hide');

    },err=>{
      $('#Model_reset').modal('hide');
      this.api.AlertMessage('Error occured','err:'+err,'error');
    })
  }


  public getuserinfo(){
    

   
  }


  


  logout(){
    localStorage.setItem('User_Status', '0');
    this.router.navigate(['/login']);

  }


}
