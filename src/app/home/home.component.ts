import { Component, OnInit } from '@angular/core';
import {ApiServiceProvider} from '../Service/api_service';
import {BaseURI} from '../Service/Constants';
import {Router, ActivatedRoute} from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
declare var $:any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ApiServiceProvider]
})
export class HomeComponent implements OnInit {

  Submenus = {'SalesMenu' : 'deactive'};
  UserName = localStorage.getItem('User_Name');
  Home_Menus = [];
  UserProfileData = [];
  To_Do_List = [];
  Profile_Pic:any = '';

  constructor(public api:ApiServiceProvider, public router:Router, private _sanitizer: DomSanitizer) { 
   
  }

  ngOnInit() {
    let Tabs_uri = BaseURI+'homePageMenus';
   // this.api.API_get(Tabs_uri).subscribe(data=>{
    this.api.API_get_auth(Tabs_uri).subscribe(data=>{
      this.Home_Menus = data;
    },err=>{
      this.api.AlertMessage('Error while getting home tabs','err:'+err,'error');
    })

    let urlpro = BaseURI+'userProfiles/3/image '

    this.api.API_get_auth(urlpro).subscribe(data=>{
      this.Profile_Pic ='data:image/jpg;base64,'+ data;
     debugger
      // this.Profile_Pic = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
      //            + data);


    },err=>{
      this.api.AlertMessage('Error while getting profile picture','err:'+err,'error');
    })


    this.GetTodo_List();
    this.getuserinfo();
  }

  public GetTodo_List(){
    let todo_get = BaseURI+'todolists/search';
    this.api.API_get_auth(todo_get).subscribe(data=>{
      this.To_Do_List = data;
    },err=>{
      this.api.AlertMessage('Error while getting to-do list','err:'+err,'error');
    })
  }

  public Remove_Todo(todo){
    let todo_get = BaseURI+'todolists/'+todo.todoListId;
    this.api.API_Delete(todo_get).subscribe(data=>{
      this.api.AlertMessage('To-Do removed successfully','Success','success');
    },err=>{
      this.api.AlertMessage('Error while deleting to-do list','err:'+err,'error');
    })
  }


  public Create_todo(values){
    

    let forgoturl = BaseURI+'todolists';
    this.api.API_AUth_post(forgoturl, values).subscribe(data=>{
      $('#add_todo').modal('hide');
      this.api.AlertMessage('New To-Do activity added succesfully','Success','success');
      this.GetTodo_List();
    },err=>{
      $('#add_todo').modal('hide');
      this.api.AlertMessage('Error occured','err:'+err,'error');
    })
  }



  public getuserinfo(){
    

    let forgoturl = BaseURI+'/userProfiles?login=dhorrairaajj';
    this.api.API_get_auth(forgoturl).subscribe(data=>{
     this.UserProfileData = data;

    },err=>{
    
      this.api.AlertMessage('Error occured','err:'+err,'error');
    })
  }

  MenuOpen(Menu_Name, index){
   

    this.router.navigate(['/section'], { queryParams: { q: Menu_Name}});

    // let Tabs_uri = BaseURI+'userMenus?homePageMenuCode='+Menu_Name;
    // this.api.API_get(Tabs_uri).subscribe(data=>{

    //  // this.Home_Menus[index].submenus = data[0].menuItems;



    // },err=>{
    //   this.api.AlertMessage('Error while getting home submenus','err:'+err,'error');
    // })
  }


  Submenu(MenuName){
    
      if(this.Submenus[MenuName] == 'active'){
        this.Submenus[MenuName] = 'deactive';
      }else{
        this.Submenus[MenuName] = 'active';
      }
     // this.Submenu[MenuName] = 'active';
  }


 

}
