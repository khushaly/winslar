import { Component, OnInit } from '@angular/core';
import {ApiServiceProvider} from '../Service/api_service';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import {BaseURI, Purchaseordercolmns} from '../Service/Constants';
import {ActivatedRoute} from '@angular/router';
import { OrderPipe } from 'ngx-order-pipe';
import { FormBuilder, FormGroup, FormArray,FormControl } from '@angular/forms';
//import { REACTIVE_FORM_DIRECTIVES } from '@angular/forms'
declare var $:any;
import { Router } from '@angular/router';   
import {ViewComponent} from '../components/ViewComponent/ViewComponent.component';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css'],
  providers: [ApiServiceProvider]
})
export class SectionComponent implements OnInit {
  order: string = '';
  reverse: boolean = false;
  menuItemCode:any = 'purchaseOrders';
  myGroup:any;
  public form: FormGroup;
  public contactList: FormArray;
  //=============================Dashboard Chart Fun_Var Ends========================================
    /*LineChart*/
    public lineChartData:Array<any> = [
      {data: [65, 59, 80, 81, 56, 55, 40], label: 'Product A'},
      {data: [28, 48, 40, 19, 86, 27, 90], label: 'Product B'},
      {data: [18, 48, 77, 9, 100, 27, 40], label: 'Product C'}
    ];
    public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
    public lineChartOptions:any = {
      responsive: true
    };
    public lineChartColors:Array<any> = [
      { // grey
        backgroundColor: 'rgba(148,159,177,0.2)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      },
      { // dark grey
        backgroundColor: 'rgba(77,83,96,0.2)',
        borderColor: 'rgba(77,83,96,1)',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)'
      },
      { // grey
        backgroundColor: 'rgba(148,159,177,0.2)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      }
    ];
    public lineChartLegend:boolean = true;
    public lineChartType:string = 'line';
  
    /*Bar Chart*/
    public barChartOptions:any = {
      scaleShowVerticalLines: false,
      responsive: true
    };
    public barChartLabels:string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    public barChartType:string = 'bar';
    public barChartLegend:boolean = true;
  
    public barChartData:any[] = [
      {data: [65, 59, 80, 81, 56, 55, 40], label: 'Product A'},
      {data: [28, 48, 40, 19, 86, 27, 90], label: 'Product B'}
    ];
  
    // Doughnut
    public doughnutChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
    public doughnutChartData:number[] = [350, 450, 100];
    public doughnutChartType:string = 'doughnut';
    public pieChartType:string = 'pie';
  
  
    // events
    public chartClicked(e:any):void {
      console.log(e);
    }
  
    public chartHovered(e:any):void {
      console.log(e);
    }

    public MenuName:any;
    public MenuPageItems = [];
    public Attributes = [];
    public Attributess = [];
    public Attribute_Keys = [];
    public Attribute_Keys_ORG = [];
    public Attributes_ORG = [];
    Single_purchase_count:any = 0;
    public Single_Record_ID_Selected:any;
    public Single_Purchase_Order_Childs:any;
     lastClickTime:number = 0;
//=============================Dashboard Chart Fun_Var Ends========================================

  public activeTab = 1;
  public TabStates:any;
  public Column_Names = [];
  public source = new LocalDataSource();
  public ADS_Fields = [];
  public Selected_Records = [];
  Single_Purchase_Order:any;
  public current_page:number = 1;
  public Branches:any = [];
  public Vendors:any = [];
  public Items:any = [];
  public item_data = [];
  public SearchProfiles = [];

  Org_Columns = {
    actions: {
      add: false,
      edit: false,
      delete: false,
      select: true,
    },
    columns: {
      id: {
        title: 'ID',
        filter:false,
        editable:false,
        "show": false
      },
      name: {
        title: 'Full Name',
        filter:true,
        "show": false
      },
      username: {
        title: 'User Name',
        filter:true,
        "show": false
      },
      email: {
        title: 'Email',
        filter:true,
        "show": false
      },
      Fields1: {
        title: 'Fields1',
        filter:true,
        "show": false
      },
      Fields2: {
        title: 'Fields2',
        filter:true,
        "show": false
      },
      Fields3: {
        title: 'Fields3',
        filter:true,
        "show": false
      },
    }
  }

  settings = {
    selectMode: 'multi',
    actions: {
      add: false,
      edit: false,
      delete: false,
      select: true,
    },
    pager: {
      display: true,
      perPage: 5
    },
    columns: {
      id: {
        title: 'ID',
        filter:false,
        editable:false,
        "show": false
      },
      name: {
        title: 'Full Name',
        filter:true,
        "show": false
      },
      username: {
        title: 'User Name',
        filter:true,
        "show": false
      },
      email: {
        title: 'Email',
        filter:true,
        "show": false
      },
     
    }
  };


  data = [];

  PurchaseOptionsCols;
  sortedPurchaseordercolmns;
  SearchData:any = [];

  constructor(public router:Router,private orderPipe:OrderPipe ,public api:ApiServiceProvider, public ng4table:Ng2SmartTableModule, public activeroute:ActivatedRoute,private fb: FormBuilder) { 
    

    this.api.LoaderApp(1);
  

    var status = localStorage.getItem('User_Status');

		if(status == "1"){
  

    this.activeroute.queryParams.subscribe(params => {
      this.MenuName = params['q'];
      console.log('register_type', this.MenuName); 
      let Tabs_uri = BaseURI+'userMenus?homePageMenuCode='+ this.MenuName;
    this.api.API_get_auth(Tabs_uri).subscribe(data=>{
      
      this.MenuPageItems = data[0].menuItems;
    },err=>{
      this.api.AlertMessage('Error while getting home submenus','err:'+err,'error');
    })
    });
  

    this.sortedPurchaseordercolmns = orderPipe.transform(this.PurchaseOptionsCols, 'attrName');
    console.log(this.sortedPurchaseordercolmns);


    let AttributesURI = BaseURI+'purchaseOrderReceipts/metadata/attributes';
    this.api.API_get_auth(AttributesURI).subscribe(data=>{
      this.Attributes = data;
      this.Attributes_ORG = data;
        console.log(this.Attributes, 'ads attr');
    //  for(var k in this.Attributes) this.Attribute_Keys.push(k.attrCd);

    for(var i=0; i<=this.Attributes.length-1;i++){
      
      this.Attribute_Keys.push(this.Attributes[i].attrCd);

      this.Attribute_Keys_ORG.push(this.Attributes[i].attrCd);
    }




    },err=>{
      this.api.AlertMessage('Error while getting Attributes','err:'+err,'error');
    });

    let SearchDataURI = BaseURI+'purchaseOrders/search';
    this.api.API_get_auth(SearchDataURI).subscribe(data=>{
      this.SearchData = data;
    },err=>{
      this.api.AlertMessage('Error while getting Attributes','err:'+err,'error');
    });

    let SearchProfileaURI = BaseURI+'searchProfiles/search?q=menuItemCode='+'Purchase Orders';
    this.api.API_get_auth(SearchProfileaURI).subscribe(data=>{
      this.SearchProfiles = data;
    },err=>{
      this.api.AlertMessage('Error while getting Attributes','err:'+err,'error');
    });


    let BrancesURI = BaseURI+'purchaseOrderReceipts/lov/branches';
    this.api.API_get_auth(BrancesURI).subscribe(data=>{
      this.Branches = data;
    },err=>{
      this.api.AlertMessage('Error while getting branches','err:'+err,'error');
    });

    let VendorURI = BaseURI+'purchaseOrderReceipts/lov/vendors';
    this.api.API_get_auth(VendorURI).subscribe(data=>{
      this.Vendors = data;
    },err=>{
      this.api.AlertMessage('Error while getting vendors','err:'+err,'error');
    });


    let ItemsURI = BaseURI+'purchaseOrderReceipts/lov/items';
    this.api.API_get_auth(ItemsURI).subscribe(data=>{
      debugger
      this.Items = data;
    },err=>{
      this.api.AlertMessage('Error while getting items','err:'+err,'error');
    });


  
    // this.myGroup = new FormGroup({
    //   attrOperators: new FormControl(),
    //   attrName: new FormControl(),
    //   attrCd: new FormControl()
    // });


  this.Column_Names = Object.keys(this.Org_Columns.columns);

    //=========All Tabs States============
    this.TabStates = {'tab1':0,'tab2':0,'tab3':0,'tab4':0,'tab99':0,'tab100':0 }

    this.PurchaseOptionsCols = Purchaseordercolmns;



  }else{
    this.router.navigate(['/login']);
    this.api.AlertMessage('Login Session Expired, Please Login Again','err:','error');
  }
}




  Pagination(type_page, value){
    
    if(type_page == 'previous'){
        if(this.current_page != 0){
          this.SearchPagination(this.current_page-1, '');
          this.current_page = this.current_page -1;
        }
    }
    else if(type_page == 'next'){
      if(this.current_page >= value){
        this.SearchPagination(this.current_page+1, '');
        this.current_page = this.current_page +1;
      }
    }
    else if(type_page == 'page_count'){
      this.current_page = value;
      this.SearchPagination(value, '');
    }
  }

  SearchPagination(page_no, sortby){
debugger
  //  http://winsarlabs.co.in/IMS/api/purchaseOrderReceipts/search?pageNo=0&pageSize=10{&q,sort}
    let SearchProfileaURI = BaseURI+'/'+this.menuItemCode+'/search?pageNo='+page_no+'&pageSize=10';
    this.api.API_get_auth(SearchProfileaURI).subscribe(data=>{
      this.SearchData = data;
    },err=>{
      this.api.AlertMessage('Error while getting Attributes','err:'+err,'error');
    });
  }

  createContact(): FormGroup {
    return this.fb.group({
      attrOperators: ['text'],
      attrName: [null],
      attrCd: [null]
    });
  }


  Add_Remove_Colmn(colmn_name, action_type){
  
    if(action_type == false){
    if (this.settings.columns[colmn_name]) {
      delete this.settings.columns[colmn_name];
      this.settings = Object.assign({}, this.settings );
    }
  }else{
     this.settings.columns[colmn_name] = this.Org_Columns.columns[colmn_name];
      this.settings = Object.assign({}, this.settings );
  }
  }


  setOrder(value: string) {
    
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }

  ngOnInit() {

    //this.api.AlertMessage('' , '' , 'success');
    this.api.API_get('').subscribe(data=>{
    },err=>{
    //  this.api.AlertMessage('Error','Err:'+err,'error');
    })

  }


  PurchaseOrder_Click(Purchaserecord){
    console.log(Purchaserecord);

    var ID_Order = Purchaserecord.purchaseOrderSeq;
    this.Single_Record_ID_Selected = Purchaserecord.purchaseOrderSeq;
    if(this.isDoubleClick()){
      this.OpenTab('tab99',99);

      let url = BaseURI+'purchaseOrderReceipts/'+ID_Order;
      this.api.API_get(url).subscribe(data=>{
          this.Single_Purchase_Order = data;
         // this.Single_purchase_count = Object.keys(data).length;
          this.Single_purchase_count = Object.keys(data);
      },err=>{

      })

    //  to get child items
      let urlchilditems = BaseURI+/purchaseOrderReceipts/+ID_Order+'/child/items';
      this.api.API_get(urlchilditems).subscribe(data=>{
          this.Single_Purchase_Order_Childs = data;
         // this.Single_purchase_count = Object.keys(data).length;
        //  this.Single_purchase_count = Object.keys(data);
      },err=>{

      })









    }
    
  }

 

  Colmn_Search(input_text, attribute){
    
          if (input_text != "") {
      var SearchData = this.SearchData.content;
      this.SearchData.content = SearchData.filter(function (el) {
          var name = el.Field;
          var text = name.toLowerCase();
          var search_text = input_text.toLowerCase();
          return text.toString().indexOf(search_text) > -1;
      }.bind(this));
    }else{
     // this.All_contact_searched = this.All_contact;
    }

  }


  onUserRowSelect(event){
    console.log(event);
  }

  // Filterby_SearchColmn(value, Field) {
  //   if (value != "") {
      
  //    this.All_contact_searched = this.All_contact.filter(function (el) {
  //         var name = el.Field;
  //         var text = name.toLowerCase();
  //         var search_text = value.toLowerCase();
  //         return text.toString().indexOf(search_text) > -1;
  //     }.bind(this));
  //   }else{
  //     this.All_contact_searched = this.All_contact;
  //   }

  //   }

  Ads_field_check(event, field, fieldobj, index_obj){

    if(event == true){
      this.Attributes[index_obj].requiredInSearch = true;
    }else{
      this.Attributes[index_obj].requiredInSearch = false;
    }
  }

  Ads_field_SearchResults(event, field, fieldobj, index_obj){

    if(event == true){
      this.Attribute_Keys.push(field);
     // this.Attributes[index_obj].requiredInSearch = true;
    }else{
      var index_ob = this.Attribute_Keys.indexOf(field);
      if (index_ob > -1) {
        this.Attribute_Keys.splice(index_ob, 1);
      }
     // this.Attributes[index_obj].requiredInSearch = false;
    }
  }





  addfilter(){
    $('#myModal').modal('hide');
    let Fields = [];
    
    for(let i=0;i<this.ADS_Fields.length;i++){
      if(this.ADS_Fields[i].requiredInFilter){
        Fields.push(this.ADS_Fields[i]);
      }
    }
    this.Attributes = Fields;
    console.log(JSON.stringify(Fields));
  }


  //========Opening and Closing Tab======================

  TabChange(Tab_no){
    this.activeTab = Tab_no;
  }


  OpenTab(tabname, tab_no){
      
      this.TabStates[tabname] = 1;
      this.activeTab = tab_no;
  }
  setData(menuItemCode){
    
    //let AttributesURI = BaseURI+menuItemCode+'/metadata/attributes';
    // let AttributesURI = BaseURI+'searchProfiles/search?q=menuItemCode='+menuItemCode+',activeFlag=Y';
    // this.api.API_get_auth(AttributesURI).subscribe(data=>{
    //   this.Attributes = data.content[0].searchCriteriaAttrDefs;
    //   this.Attributess = data.content[0].searchResultAttrDefs
    // },err=>{
    //   this.api.AlertMessage('Error while getting Attributes','err:'+err,'error');
    // });

    menuItemCode = menuItemCode.replace(/\s/g,"");
    menuItemCode = menuItemCode[0].toLowerCase() + menuItemCode.slice(1);
    this.menuItemCode = menuItemCode;
    let SearchDataURI = BaseURI+menuItemCode+'/search';
    this.api.API_get_auth(SearchDataURI).subscribe(data=>{
      this.SearchData = data;
    },err=>{
      this.api.AlertMessage('Error while getting Attributes','err:'+err,'error');
    });

    let Attributes_URI = BaseURI+menuItemCode+'/metadata/attributes';
    this.api.API_get_auth(Attributes_URI).subscribe(data=>{
      this.ADS_Fields = data
    },err=>{
      this.api.AlertMessage('Error while getting Attributes','err:'+err,'error');
    });
  }

  public SearchData_API(values) {
   
    debugger
        
    var SearchDataURI = BaseURI+this.menuItemCode+'/search?q=';

      var count = Object.keys(values).length;

    // for(var v=1; v<=count;v++){
    //   let str = v+'='+values[v]+'&';
    //  var SearchDataURI= SearchDataURI.concat(str);
    //   console.log(SearchDataURI)
    // }

    for ( var property in values ) {
      let str = property+'='+values[property]+'&';
      var SearchDataURI= SearchDataURI.concat(str);
       console.log(SearchDataURI)
    }

    this.api.API_get_auth(SearchDataURI).subscribe(data=>{
      this.SearchData = data;

       


        console.log(this.Attribute_Keys, 'all keys');
    },err=>{
      this.api.AlertMessage('Error while getting Attributes','err:'+err,'error');
    });

  }

  public FilterData_API(text, field) {
   
        
    var SearchDataURI = BaseURI+this.menuItemCode+'/search?q='+field+'='+text;


    this.api.API_get_auth(SearchDataURI).subscribe(data=>{
      this.SearchData = data;

 
    },err=>{
      this.api.AlertMessage('Error while getting Attributes','err:'+err,'error');
    });

  }

  public SORTData_API(postion, field) {
   
    var SearchDataURI = BaseURI+this.menuItemCode+'/search?q=sort='+field+' '+postion;
    this.api.API_get_auth(SearchDataURI).subscribe(data=>{
      this.SearchData = data;
    },err=>{
      this.api.AlertMessage('Error while getting Attributes','err:'+err,'error');
    });

  }




  CloseTab(Tab_no, tab_no){
   // this.activeTab
    if(this.activeTab == tab_no){
      this.activeTab = 1;
    }
    var tabno = Tab_no;
    this.TabStates[tabno] = 0;

  }
  //======================================================

  SaveSearch(formvalue){

    let data =   {
        menuItemCode: "Purchase Orders",
        profileName: formvalue.save_name,
        pageCode: "VIEW",
        activeFlag: "Y",
        searchResultAttrDefs: []
  }

      var SaveSearchDataURI = BaseURI+'/searchProfiles';
      this.api.API_AUth_post(SaveSearchDataURI, data).subscribe(data=>{
        $('#SavedSearch').modal('hide')
        this.api.AlertMessage('Search Profile Added','Success','success');
      //  this.SearchData = data;
      },err=>{
        this.api.AlertMessage('Error while getting Attributes','err:'+err,'error');
      });


  }

   isDoubleClick():boolean{

    var dc:boolean = false;

    if (this.lastClickTime === 0){
      this.lastClickTime = new Date().getTime();
    }
    else{
        dc = ( ((new Date().getTime()) - this.lastClickTime) < 400);
        this.lastClickTime = 0; 
    }
    return dc;
}

//===========================Delete Data========================================
//=========================================================================================
Delete_now(){
if(this.Selected_Records.length == 0){
  this.api.AlertMessage('Please Select Purchase Order','err:','error');
}else{
  let Delete_URI = BaseURI+this.menuItemCode+'/'+this.Selected_Records[0];
  this.api.API_Delete(Delete_URI).subscribe(data=>{
    this.api.AlertMessage('Record Deleted Successfully','Success','success');
   // this.ADS_Fields = data
  },err=>{
    this.api.AlertMessage('Error while getting Attributes','err:'+err,'error');
  });
}

}

Delete_record(status,record_data){

  if(status == true){
    this.Selected_Records.push(record_data.purchaseOrderSeq);
  }else{

  }


 

}

//===========================Update / Edit Section========================================
//=========================================================================================

Update_Purchase_Order(values){
  var UpdateDataURI = BaseURI+this.menuItemCode+'/'+this.Single_Record_ID_Selected;
  debugger
    this.api.API_put(UpdateDataURI, values).subscribe(data=>{
      
    //  this.SearchData = data;
    this.api.AlertMessage('Record Updated Successfully', 'success', 'success');
    },err=>{
      this.api.AlertMessage('Error while updating record','err:'+err,'error');
    });
}


Add_Purchase_Order(value_form){

 

 var dataPOR =  {
    "branchCd": "DR",
       "branchSeqForm": 10,
       "branchName": value_form.branchName,
       "vendorSeqForm": 32,
       "vendorCd": "DEFAULT-MFG-VENDOR",
       "vendorName": value_form.vendorName,
       "requestedDateForm": '22-Dec-19'
}

  
  var addDataURI = BaseURI+'purchaseOrderReceipts/';
    this.api.API_AUth_post(addDataURI, dataPOR).subscribe(data=>{
      
    //  this.SearchData = data;
    this.api.AlertMessage('Record Added Successfully', 'success', 'success');
    },err=>{
      this.api.AlertMessage('Error while updating record','err:'+err,'error');
    });
}

Order_Item_Selected(itemdata){
  debugger
  //item_data
}

Add_Order_INPOR(values){

//   {
//     "purchaseOrderItemSeq": 223,
//      "itemSeqForm": 1920,
//      "itemID": "000004",

// }

  values.purchaseOrderItemSeq = this.Single_Record_ID_Selected;
  //values.itemSeqForm = 110
  values.itemID = values.itemSeqForm

  var addDataURI = BaseURI+'purchaseOrderReceipts/'+this.Single_Record_ID_Selected+'/child/items';
  this.api.API_AUth_post(addDataURI, values).subscribe(data=>{
    
  //  this.SearchData = data;
  this.api.AlertMessage('Record Added Successfully', 'success', 'success');
    $('#AddOrderPOR_Model').modal('hide');
  let urlchilditems = BaseURI+/purchaseOrderReceipts/+this.Single_Record_ID_Selected+'/child/items';
  this.api.API_get(urlchilditems).subscribe(data=>{
      this.Single_Purchase_Order_Childs = data;
     // this.Single_purchase_count = Object.keys(data).length;
    //  this.Single_purchase_count = Object.keys(data);
  },err=>{

  })


  },err=>{
    this.api.AlertMessage('Error while updating record','err:'+err,'error');
  });
}


}
