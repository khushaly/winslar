import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import { Response, Headers, Http, RequestOptions} from '@angular/http';
import { Observable } from "rxjs/Observable";
import Swal from 'sweetalert2'


declare var $: any;

@Injectable()

export class ApiServiceProvider {
	public Global_header = new Headers();
	public options:any;
	
	constructor(public http:Http, public router:Router){
		this.options = new RequestOptions({ headers: this.Global_header });
    }

	ngOnInit() {

	}

	//-------------------------------PUBLIC API CALL METHODs----------------------------------------------------



	API_get(url){

	var status = localStorage.getItem('User_Status');

		if(status == "1"){
			let headersend  = new Headers();
			let username  =localStorage.getItem('User_Name');
			let password  =localStorage.getItem('password');
			headersend.append("Content-Type", "application/x-www-form-urlencoded");
			headersend.append("Authorization", "Basic " + btoa(username + ":" + password));
			let options = new RequestOptions({ headers: headersend, withCredentials: true });
	
			return this.http.get(url , options)
			.map((res: Response) => {
			  var resp = res.json();
			  return resp;
			})
		}
		else{
			this.AlertMessage('Login Session Expired, Please Login Again','err:','error');
			this.router.navigate(['/login']);
		}

	
	}

	API_get_auth(url){
		var status = localStorage.getItem('User_Status');

		if(status == "1"){

		let headersend  = new Headers();
		let username  =localStorage.getItem('User_Name');
    	let password  =localStorage.getItem('password');
		headersend.append("Content-Type", "application/x-www-form-urlencoded");
		headersend.append("Authorization", "Basic " + btoa(username + ":" + password));
		let options = new RequestOptions({ headers: headersend, withCredentials: true });

		return this.http.get(url , options)
		.map((res: Response) => {
		  var resp = res.json();
		  return resp;
		})
	}

		else{
			this.AlertMessage('Login Session Expired, Please Login Again','err:','error');
			this.router.navigate(['/login']);
		}

	}

	API_post(url, data){

		return this.http.post(url ,data , this.options)
		.map((res: Response) => {
		  var resp = res.json();
		  return resp;
		})
	}

	API_AUth_post(url, data){

		let headersend  = new Headers();
		let username  =localStorage.getItem('User_Name');
    	let password  =localStorage.getItem('password');
	//	headersend.append("Content-Type", "application/x-www-form-urlencoded");
		headersend.append("Authorization", "Basic " + btoa(username + ":" + password));
		let options = new RequestOptions({ headers: headersend, withCredentials: true });
debugger
		return this.http.post(url ,data ,options)
		.map((res: Response) => {
		  var resp = res.json();
		  return resp;
		})
	}




	API_put(url, data){
		let headersend  = new Headers();
		let username  =localStorage.getItem('User_Name');
    	let password  =localStorage.getItem('password');
		headersend.append("Content-Type", "application/x-www-form-urlencoded");
		headersend.append("Authorization", "Basic " + btoa(username + ":" + password));
		let options = new RequestOptions({ headers: headersend, withCredentials: true });

		return this.http.put(url,data , options)
		.map((res: Response) => {
		  var resp = res.json();
		  return resp;
		})
	}

	API_Delete(url){
		let headersend  = new Headers();
		let username  =localStorage.getItem('User_Name');
    	let password  =localStorage.getItem('password');
		headersend.append("Content-Type", "application/x-www-form-urlencoded");
		headersend.append("Authorization", "Basic " + btoa(username + ":" + password));
		let options = new RequestOptions({ headers: headersend, withCredentials: true });


		return this.http.delete(url , options)
		.map((res: Response) => {
		  var resp = res.json();
		  return resp;
		})
	}


	API_FORM_POST(url: string, body?: any) {

		let bodysend = new URLSearchParams();
		for (var key in body) {
			
			bodysend.append(key, body[key]);
		}
		let headersend  = new Headers()
		headersend.append("Content-Type", "application/json");
		let options = new RequestOptions({ headers: headersend, withCredentials: true });
	   	return this.http.post(url,bodysend.toString(), options)
	   .map((res: Response) => {
		var resp = res.json();
		return resp;
	   })

	}


	API_FORM_POST_File(url: string ,body?: any) {
	
		var form_data = new FormData();
		for (var key in body) {  form_data.append(key, body[key]);  }		

		   return this.http.post(url,form_data, this.options)
	   		.map((res: Response) => {
		 	return res;
	   })

	}


	errorHandler(error: any) {
		var resp = error.json();
		var errorObjBody = error.text();
		var errorObj = JSON.parse(errorObjBody);
		return Observable.throw(errorObj);
	  }





	//-------------------------------PUBLIC API METHODs END----------------------------------------------------	


	errorcommon(){
		$('#Model_error').modal('show');
	}

	LoaderApp(switch_ty){
		if(switch_ty == 1){
		$('#App_Loader').modal('show');
		}else{
			$('#App_Loader').modal('hide');
		}
	}

	AlertMessage(heading, message, type){
		Swal(heading, message, type);
	}





}

