
export const BaseURI = 'IMS/api/';
export const BaseURI_Org = 'http://winsarlabs.co.in/IMS/api/';


export var APIURL = Object.freeze({
  

})


 export var Purchaseordercolmns = 

    [
        {
            "objectType": "Purchases",
            "attrName": "Order#",
            "attrCd": "purchaseOrderNo",
            "attrTypeCd": "Long",
            "requiredInSearch": true,
            "attrDisplayOrder": 1,
            "attrCdFilter": "purchaseOrderNo",
            "attrTypeCdFilter": "Long",
            "requiredInFilter": true,
            "attrDisplayOrderFilter": 1,
            "operators": [
                "=",
                "<>",
                "<",
                ">",
                "<=",
                ">="
            ]
        },
        {
            "objectType": "Purchases",
            "attrName": "Branch Name",
            "attrCd": "branchName",
            "attrTypeCd": "String",
            "requiredInSearch": true,
            "attrDisplayOrder": 2,
            "attrCdFilter": "branch.branchName",
            "attrTypeCdFilter": "String",
            "requiredInFilter": true,
            "attrDisplayOrderFilter": 2,
            "operators": [
                "Contains",
                "StartsWith",
                "Equals",
                "EndsWith"
            ]
        },
        {
            "objectType": "Purchases",
            "attrName": "Vendor",
            "attrCd": "vendorName",
            "attrTypeCd": "String",
            "requiredInSearch": true,
            "attrDisplayOrder": 3,
            "attrCdFilter": "vendor.vendorName",
            "attrTypeCdFilter": "String",
            "requiredInFilter": false,
            "attrDisplayOrderFilter": 3,
            "operators": [
                "Contains",
                "StartsWith",
                "Equals",
                "EndsWith"
            ]
        },
        {
            "objectType": "Purchases",
            "attrName": "Requested By",
            "attrCd": "requestedBy",
            "attrTypeCd": "String",
            "requiredInSearch": false,
            "attrDisplayOrder": 4,
            "attrCdFilter": "requestedBy",
            "attrTypeCdFilter": "String",
            "requiredInFilter": false,
            "attrDisplayOrderFilter": 4,
            "operators": [
                "Contains",
                "StartsWith",
                "Equals",
                "EndsWith"
            ]
        },
        {
            "objectType": "Purchases",
            "attrName": "Requested Date",
            "attrCd": "requestedDateForm",
            "attrTypeCd": "String",
            "requiredInSearch": true,
            "attrDisplayOrder": 5,
            "attrCdFilter": "requestDate",
            "attrTypeCdFilter": "Date",
            "requiredInFilter": true,
            "attrDisplayOrderFilter": 5,
            "operators": [
                "=",
                "<>",
                "<",
                ">",
                "<=",
                ">="
            ]
        },
        {
            "objectType": "Purchases",
            "attrName": "Received By",
            "attrCd": "receivedBy",
            "attrTypeCd": "String",
            "requiredInSearch": true,
            "attrDisplayOrder": 5,
            "attrCdFilter": "receivedBy",
            "attrTypeCdFilter": "String",
            "requiredInFilter": false,
            "attrDisplayOrderFilter": 5,
            "operators": [
                "Contains",
                "StartsWith",
                "Equals",
                "EndsWith"
            ]
        },
        {
            "objectType": "Purchases",
            "attrName": "Received Date",
            "attrCd": "receivedDateTime",
            "attrTypeCd": "String",
            "requiredInSearch": false,
            "attrDisplayOrder": 6,
            "attrCdFilter": "requestDate",
            "attrTypeCdFilter": "Date",
            "requiredInFilter": false,
            "attrDisplayOrderFilter": 6,
            "operators": [
                "=",
                "<>",
                "<",
                ">",
                "<=",
                ">="
            ]
        },
        {
            "objectType": "Purchases",
            "attrName": "Reference#",
            "attrCd": "referenceNo",
            "attrTypeCd": "String",
            "requiredInSearch": true,
            "attrDisplayOrder": 7,
            "attrCdFilter": "referenceNo",
            "attrTypeCdFilter": "String",
            "requiredInFilter": false,
            "attrDisplayOrderFilter": 7,
            "operators": [
                "Contains",
                "StartsWith",
                "Equals",
                "EndsWith"
            ]
        },
        {
            "objectType": "Purchases",
            "attrName": "Status Name",
            "attrCd": "statusName",
            "attrTypeCd": "String",
            "requiredInSearch": true,
            "attrDisplayOrder": 8,
            "attrCdFilter": "status.name",
            "attrTypeCdFilter": "String",
            "requiredInFilter": false,
            "attrDisplayOrderFilter": 8,
            "operators": [
                "Contains",
                "StartsWith",
                "Equals",
                "EndsWith"
            ]
        }
    ]
 